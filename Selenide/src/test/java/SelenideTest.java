import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest {

    @Test
    public void selenideTest(){
        open("https://kharkiv.ithillel.ua/");
    }

    @Test
    public void selenideTest1(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"signCoursesButton\"]")).click();
        $(By.xpath("//*[@id=\"signCourses\"]/div/form/div/div[2]")).should(Condition.appear);
    }

    @Test
    public void selenideTest2(){
        open("https://kharkiv.ithillel.ua/");
        $(By.xpath("//*[@id=\"school\"]")).click();
        $(By.xpath("//*[@id=\"school-menu\"]/div/ul/li[3]/a")).should(Condition.appear);
    }

    @Test
    public void selenideTest3(){
        open("https://ithillel.ua/");
        $(By.xpath("//*[@id=\"cityModal\"]/header/p[1]")).should(Condition.appear);
        $(By.xpath("//*[@id=\"cityModalLink\"]")).click();
        $(By.xpath("//*[@id=\"headerCities\"]/a[4]")).shouldBe(Condition.visible);
    }

    @Test
    public void selenideTest4(){
        open("https://ithillel.ua/");
        $(By.xpath("/html/body/section[1]/div/article/div/div/figure/img")).isDisplayed();
    }
}
